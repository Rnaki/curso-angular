const { type } = require("os");

describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
      cy.visit('http://localhost:4200');
      cy.contains('angular-whishlist');
      cy.get('h1 b').should('contain', 'HOLA es');
      cy.get('form').within(($form) => {
        cy.get('#nombre').type('Chile')
        cy.root().submit()
        cy.get('#nombre').clear()
        cy.get('#nombre').type('Iquique')
        cy.root().submit()
        cy.get('#nombre').clear()
    })
})
});
